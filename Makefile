default: *
	make run

run: *
	make sass
	pipenv run fbs run

freeze: *
	make sass
	pipenv run fbs freeze

SCSS = $(wildcard src/scss/*.scss)
sass: $(SCSS)
	sass --no-source-map --update src/scss/main.scss src/main/resources/base/main.qss
