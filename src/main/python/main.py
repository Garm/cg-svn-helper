from fbs_runtime.application_context import ApplicationContext
from PySide2.QtWidgets import QMainWindow
import qdarkstyle
from widgets.RepoSelection import RepoSelection
import utils

import sys

class AppContext(ApplicationContext):           # 1. Subclass ApplicationContext
    def run(self):                              # 2. Implement run()
        window = QMainWindow()
        window.setWindowTitle("CG SVN Helper")
        window.resize(300, 540)
        style = qdarkstyle.load_stylesheet_pyside2()
        style += utils.load_stylesheet(self.get_resource("main.qss"))
        window.setStyleSheet(style)

        self.repo_selection = RepoSelection(window)
        self.repo_selection.resize(300, 540)
        self.repo_selection.show()

        window.show()
        return self.app.exec_()                 # 3. End run() with this line

if __name__ == '__main__':
    appctxt = AppContext()                      # 4. Instantiate the subclass
    exit_code = appctxt.run()                   # 5. Invoke run()
    sys.exit(exit_code)