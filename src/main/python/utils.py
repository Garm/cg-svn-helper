from fbs_runtime.application_context import ApplicationContext

def load_stylesheet(path):
    with open(path, "r") as file:
        style = file.read();
    return style