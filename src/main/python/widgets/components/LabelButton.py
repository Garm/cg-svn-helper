from PySide2.QtWidgets import QPushButton

class LabelButton(QPushButton):
    def __init__(self, parent=None):
        super(LabelButton, self).__init__(parent)
        
        self.setFixedHeight(36)
        self.setStyleSheet("QPushButton{ font-size: 18px; }")