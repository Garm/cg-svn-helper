from PySide2.QtWidgets import QDialog, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, QSizePolicy
from PySide2.QtCore import Qt

class CheckoutDialog(QDialog):
    def __init__(self, parent=None):
        super(CheckoutDialog, self).__init__(parent)

        self.setWindowTitle("Checkout Remote")

        layout = QVBoxLayout()
        
        user_layout = QHBoxLayout()

        user_label = QLabel("User")
        user_label.setFixedWidth(75)
        user_layout.addWidget(user_label)

        user_edit = QLineEdit()
        user_layout.addWidget(user_edit)

        layout.addLayout(user_layout)
        
        password_layout = QHBoxLayout()

        password_label = QLabel("Password")
        password_label.setFixedWidth(75)
        password_layout.addWidget(password_label)

        password_edit = QLineEdit()
        password_edit.setEchoMode(QLineEdit.EchoMode.Password)
        password_layout.addWidget(password_edit)

        layout.addLayout(password_layout)

        location_layout = QHBoxLayout()

        location_label = QLabel("Location")
        location_label.setFixedWidth(75)
        location_label.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        location_layout.addWidget(location_label)

        location_edit = QLineEdit()
        location_edit.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        location_layout.addWidget(location_edit)

        location_button = QPushButton("Browse")
        location_button.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Preferred)
        location_layout.addWidget(location_button)

        layout.addLayout(location_layout)

        button_layout = QHBoxLayout()
        button_layout.setSpacing(0)

        button_confirm = QPushButton("Confirm")
        button_confirm.clicked.connect(lambda _: self.done(QDialog.DialogCode.Accepted))
        button_layout.addWidget(button_confirm)

        button_cancel= QPushButton("Cancel")
        button_cancel.clicked.connect(lambda _: self.done(QDialog.DialogCode.Rejected))
        button_layout.addWidget(button_cancel)

        layout.addLayout(button_layout)
        
        self.setLayout(layout)

        self.location = location_edit
    
    def get_location(self):
        return self.location.text()
