from PySide2.QtWidgets import QWidget, QLabel, QVBoxLayout, QPushButton, QDialog, QFormLayout
from PySide2.QtCore import Qt
from .components.LabelButton import LabelButton
from .components.CheckoutDialog import CheckoutDialog
from svn.remote import RemoteClient

class RepoSelection(QWidget):
    def __init__(self, parent=None):
        super(RepoSelection, self).__init__(parent)

        layout = QVBoxLayout()
        layout.setSpacing(0)

        title = QLabel("CG SVN Helper")
        title.setFixedHeight(48)
        title.setStyleSheet("QLabel { font-size: 36px; font-weight: bold; }")
        layout.addWidget(title)

        layout.addLayout(self.create_start())
        layout.addLayout(self.create_recents())

        self.setLayout(layout)
    
    def create_start(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)

        label = QLabel("Start")
        label.setFixedHeight(36)
        label.setStyleSheet("QLabel { font-size: 24px; }")
        layout.addWidget(label)

        checkout = LabelButton()
        checkout.setText("Checkout Remote")
        def checkout_dialog():
            dialog = CheckoutDialog(self)
            if dialog.exec_():
                print("Location:", dialog.get_location())
        checkout.clicked.connect(checkout_dialog)
        layout.addWidget(checkout)

        checkout = LabelButton()
        checkout.setText("Open Local")
        layout.addWidget(checkout)

        return layout

    def create_recents(self):
        layout = QVBoxLayout()
        layout.setSpacing(0)

        label = QLabel("Recents")
        label.setFixedHeight(24)
        label.setStyleSheet("QLabel { font-size: 24px; }")
        layout.addWidget(label)

        layout.addStretch()
        return layout
    